#!/usr/bin/perl
use strict;
use warnings;

print "Running perl code\n";
print "Starting to call external bash script\n";

# Sample Argument to be passed
my $my_arg = "ARG1";

#system("sh", "bash-script.sh");
# With arguments
system("sh", "bash-script.sh","$my_arg");

print "Back to parent perl script\n";

