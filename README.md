# Perl for Dummies.

Welcome to the perl for dummies tutorial. 
PS: This is at the moment more like a cheathseet. 

## The first lines of Perl code - Hellow World Program

```
#!/usr/bin/perl
use strict;
use warnings;

print "Hello World\n"
```

### Output:
```
Hello World
```

### Variables in Perl

- There are few types of variables in perl:

| Type   | Description |
|--|--|
| Scalar | Any one datatype - Int, Float, Strings etc|
| Arrays | Ordered list of scalars - Starts with @ | 
| Hashes | Key value pairs - Starts with % |


### Variables

```
#!/usr/bin/perl
my $website = "TechAntidote.com";
print "$website\n"; 
```

### Arrays - Example

-Below is a simple array example:

```
#!/usr/bin/perl
my @names = ("John Wick", "Batman", "Superman");
print "$names\n"; 
```

### Arrays in Perl - Example 2 


- Print all elements in an array

```
#!/usr/bin/perl

@rcodes = (0,1,2,3,4,5,6,7,8);
print "@rcodes \n";
```


- Define array seperated by whitespace 
```
@rcode_name = qw/NOERROR SERVFAIL FORMERR NXDOMAIN/;
print @rcode_name;
```

- Get the lenght of an array

```
print "Size of Array:\t", scalar @rcodes, "\n";
my $size_of_array = scalar @rcodes;
print "Size Var = ", "$size_of_array", "\n";
```


## Sources/References/Credits

- Below are the links that helped me in building this guide. Credits to them!! :)

https://www.tutorialspoint.com/perl/index.htm

- Perl Logo : www.perl.com
